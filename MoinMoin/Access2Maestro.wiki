## page was renamed from test
#acl YanisBaour:read,write,delete,revert,admin PhilippeGallais:read JeanMichelHernandez:read


 /* YanisBaour */

 [[http://10.111.200.107/wikinoc/QoESondesActivesWitbe|{{attachment:logo-witbe.png|width=30|align="left"}}]]

<<BR>> <<BR>> 


{{{#!html
  <font size="6" style="margin-left: 50px;"> <B> Support Witbe </B></font>
}}}

<<BR>> <<BR>> ---------- <<BR>> <<BR>> 


{{{#!html
  <center>
}}}

<<TableOfContents(3)>>

{{{#!html
  </center>
}}}

<<BR>> <<BR>> <<BR>> 


= Accès au portail d'administration =
   * `URL` : http:///[MaestroIP]/admin
   * `Login` : maestro@witbe.net
   * `Password` : maestro77

== Groupes ==

{{attachment:maestro.png|authentification|align="right"}}


* Associer des autorisations de lecture et d'écriture à différents utilisateurs 
* Restreindre l'accès à certaines ressources à certains utilisateurs
* Il y a 4 types de groupes
   * '''Admin''' peut tout faire
   * '''RO''' "Read-Only" - droits de lecture seulement
   * '''RW''' "Read-Write" - peut gérer et lire toutes les ressources, mais les utilisateurs et les groupes qui sont toujours en mode "Read-Only"
   * '''LDAP''' Équivalent aux droits "Read-Only". Par défaut attribué aux utilisateurs qui se connectent via LDAP

{{attachment:main-page-maestro.png|main|width=1100}}


== Utilisateurs ==


* Un utilisateur est simplement une personne qui pourra se connecter à :
  * `Maestro`
  * `Workbench`

* Pour les personnes qui souhaitent voir les dashs sur le Datalab, le compte doit être créé directement sur le Datalab : `Outils` -> `Server Admin` -> `Add New User`  puis l'affecter à l'organisation ''dev''
=== Créer un compte utilisateur ===


 {{attachment:add-user.png|logo|align="center"}}

=== liste des utilisateurs ===

 {{attachment:user-list.png|users liste|align="center"}}

=== Définir les détails de l'utilisateur ===

 {{attachment:define-user-details.png|Define user details|width=500}}
 {{attachment:user-details.png|user details|width=500}}


=== LOCKS ===

Si une ressource du Workbench est verrouillée par un utilisateur, vous pouvez déverrouiller l'appareil depuis cet emplacement.

 {{attachment:locks.png|locks|align="left"}}

<<BR>><<BR>><<BR>><<BR>>
<<BR>><<BR>><<BR>><<BR>>
<<BR>><<BR>><<BR>><<BR>>
<<BR>><<BR>><<BR>><<BR>>
<<BR>><<BR>><<BR>><<BR>>
