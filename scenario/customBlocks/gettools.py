### begin wb_initialization
### end wb_initialization
### begin wb_common
### end wb_common
### begin wb_destroy
### end wb_destroy

@wb_export
def GetMyDeviceName(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"e5f41ffc-02e4-407a-b5d8-f7be23cbacb4","in":{},"out":{"myDeviceName":"string"},"comments":{"in":{},"out":{"myDeviceName":""}}}"""
    import urllib
    import urllib2
    from urllib2 import Request, urlopen
    import json


    ret_dict = {}

    print("devuice UUId %s" %d["environment"]["device_uuid"])
    request = Request('http://127.0.0.1/api/v2/devices/' + d["environment"]["device_uuid"]) # call device API
    response_body = urlopen(request).read()
    output = json.loads(response_body)

    myDevice = json.loads(output["properties"])

    print("mydevice :%s " %myDevice["name"])
    ret_dict["myDeviceName"]= str(myDevice["name"])
    ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
    return ret_dict


@wb_export
def Remove_special_characters(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"f52a2859-5776-458a-9be8-d8aefaf88b8a","in":{"textToFilter":"string","acceptedChars":"string"},"out":{"filteredText":"string"},"comments":{"in":{"textToFilter":"text to filter","acceptedChars":"Accepted Chars"},"out":{"filteredText":"Filtered Text"}}}"""
    import re
    textToFilter = d["textToFilter"]
    acceptedChars = d["acceptedChars"] 
    filteredText = ""

    for character in textToFilter :
       if character in acceptedChars :
          filteredText = filteredText + character

    filteredText = re.sub("([\\.\\-_ \\*])*$", "",filteredText)

    print "Filtered Text is : " + filteredText

    ret_dict = {}
    ret_dict["filteredText"] = filteredText
    ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output

    return ret_dict

@wb_export
def correctDetectedErrors(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"8d358bbb-7fb0-470e-9cfc-1b26be56aa88","in":{"textDetected":"string"},"out":{"textExpected":"string"},"comments":{"in":{"textDetected":""},"out":{"textExpected":""}}}"""
    # You code here
    ret_dict = {}

    chainToProcess = ""

    chainToProcess = d["textDetected"]

    chainToProcess = chainToProcess.replace(':','') ## remove the ":" character at the beginning of the string
    chainToProcess = chainToProcess.lstrip() ## remove the spaces at the beginning and at the end of the chain


    # Correct OCR
    switcher = {
        'A64': 'AG4',
        'A62': 'AG2',
        '5M0': 'SM0',
        '530': 'S30',
        'AGO': 'AG0',
        'GEO': 'GE0',
        'GAO': 'GA0',
        'PARTOB': 'PART06',
        'PARTOB-I': 'PART06-1',
        'VODENTO0996': 'VODENT0996',
        'VODENTO0072': 'VODENT0072',
        'PARTOI-I': 'PART01-1',
        'PARTOI-O': 'PART01-0',
        'DHO': 'DH0',
        'A63': 'AG3',
        'AGZ': 'AG2',
        'A61': 'AG1',
        'GA]': 'GA1',
        'ERR]': 'ERR1',
        'TUO': 'TU0',
        'GAZ': 'GA2',
        '083': 'C83',
        'CODE D\'ERREUR : ERREUR LORS DE L\'APPEL DU SERVICE\nASGARD.': 'ERREUR LORS DE L\'APPEL DU SERVICE ASGARD',
        'CB3': 'C83',
        'PARTOS': 'PART06',
        'PARTOS-1': 'PART06-1',
        'PCS': 'PC9',
        '5V1': 'SV1',
        'c83': 'C83',
        'Cc83': 'C83',  
        'PARTO02-0': 'PART02-0', 
        'PARTO3': 'PART03', 
        'PARTO06-1': 'PART06-1',
        'PARTO06-0': 'PART06-0',
        'PARTO02-0': 'PART02-0',
        '11810': 'TU0',
        'GAl': 'GA1',
        '70': 'GA0',
        '- TUT':'TU1', 
        'LYY':'TU2',
        'PARTO6':'PART06',
        '5M2': 'SM2',
        'VODENTO0084':'VODENT0084',
        'PARTO1-0':'PART01-0',
        'PARTO1-1':'PART01-1'
    }
    print(switcher.get(chainToProcess, chainToProcess))

    ret_dict["textExpected"] = switcher.get(chainToProcess, chainToProcess)


    ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
    return ret_dict