### begin wb_initialization
### end wb_initialization
### begin wb_common
### end wb_common
### begin wb_destroy
### end wb_destroy

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

@wb_export
def generateRandomIntNumber(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"569121f7-c16a-4798-a460-cb179a2ef01e","in":{"minRange":"integer","maxRange":"integer"},"out":{"randomIntNumber":"integer"},"comments":{"in":{"minRange":"min range value","maxRange":"max range value"},"out":{"randomIntNumber":"random value chosen between maxRange and minRange"}}}"""
	import random
	minRange = d["minRange"]
	maxRange = d["maxRange"]

	# You code here
	ret_dict = {}
	ret_dict["randomIntNumber"] = random.randint(minRange,maxRange)
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict

@wb_export
def si(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"bc9184ac-0df9-4bce-8b49-866981997db5","in":{"myChoice":"boolean"},"out":{},"comments":{"in":{"myChoice":""},"out":{}}}"""
	# Your code here

	ret_dict = {}

	if d["myChoice"] == 1: 
		ret_dict["returncode"] = 0 # SUCCESS output
		print("Go Go Go")
	else :
		ret_dict["returncode"] = 1 # FAILURE output
		print("No Go")

	return ret_dict

@wb_export
def Compare2Expressions(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"68d2d3bf-db01-45cd-8be2-53d9674bd48c","in":{"myLeftOperand":"integer","myRightOperand":"integer","myOperator":"string"},"out":{},"comments":{"in":{"myLeftOperand":"","myRightOperand":"","myOperator":""},"out":{}}}"""
	import operator

	ret_dict = {}

	myOperator = {
	    '<': operator.lt,
	    '<=': operator.le,
	    '==': operator.eq,
	    '!=': operator.ne,
	    '>=': operator.ge,
	    '>': operator.gt
	}

	myOperation = myOperator.get(d["myOperator"])
	myCondition=myOperation(d["myLeftOperand"], d["myRightOperand"])


	if myCondition :
	    print("Condition True")
	    ret_dict["returncode"] = 0
	else :
	    ret_dict["returncode"] = 1

	return ret_dict


@wb_export
def IncrementationValue(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"5f3aadc6-e084-42cf-b1c6-097aa0ae6c42","in":{"myValue":"integer","myNumberToAdd":"integer"},"out":{"myNewValue":"integer"},"comments":{"in":{"myValue":"","myNumberToAdd":""},"out":{"myNewValue":""}}}"""

	ret_dict = {}
	ret_dict["myNewValue"] = int(d["myValue"]) + int(d["myNumberToAdd"])
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict

@wb_export
def calculateRunScore(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"d3a57af6-48e9-4509-9a7f-f9aa929f432e","in":{},"out":{"runScore":"integer"},"comments":{"in":{},"out":{"runScore":"use for TNQ calculation"}}}"""
	# You code here
	ret_dict = {}


	# To use the input params, simply use d["timeRead"] d["Format24h"] d["MinuteDifferenceThreshold"] 
	# Your code here

	#timeRead = d["timeRead"]
	#Format24h = d["Format24h"]
	#MinuteDifferenceThreshold = d["MinuteDifferenceThreshold"]

	# Evaluation of the local time, as seen on screen
	import time
	import datetime
	import pytz

	"""
	3 ponderations ont ete mis en place. Car le trafic par jours est de 60% en heures charges et 40% en heures creuses.
	Le trafic de la semaine (lundi au dimanche) est de 60% le week-end et de 40% les jours de semaine. 

	lundi au vendredi tous les jours de 00h00 a 19h59 = Periode Creuse VERT -> 16% du traffic le runScore sera de 16 points

	lundi au jeudi tous les jours de 20h00 a 23h59 = Alerte ORANGE -> 24% du traffic le runScore sera de 24 points
	samedi au dimanche tous les jours de 00h00 a 19h59 = Alerte ORANGE -> 24% du traffic le runScore sera de 24 points

	Tous les vendredi de 20h00 a 23h59 = Alerte ROUGE -> 36% du traffic le runScore sera de 36 points
	Tous les samedi et dimanche de 20h00 a 23h59 = Alerte ROUGE -> 36% du traffic le runScore sera de 36 points
	"""

	offPeakHourWeekday = [(0,4),(00,00),(19,59)] #GREEN ALERT - 40% du traffic
	peakHourWeekday = [(0,3),(20,00),(23,59)] #ORANGE ALERT - 40% du traffic
	peakHourWeekend = [(5,6),(00,00),(19,59)] #ORANGE ALERT - 60% du traffic
	criticPeakHour = [(4,6),(20,00),(23,59)] #RED ALERT - 60% du traffic

	#Definition des ponderations :
	ponderation = [16,24,36]
	lowRate = ponderation[0]
	middleRate = ponderation[1]
	highRate = ponderation[2]

	#Definition des periodes de ponderations :
	print "----------------------OFF PEAK HOUR WEEKDAY----------------------"
	startOffPeakWeekday = offPeakHourWeekday[0][0]
	print 'startOffPeakWeekday : %s' %startOffPeakWeekday
	endOffPeakWeekday = offPeakHourWeekday[0][1]
	print 'endOffPeakWeekday : %s' %endOffPeakWeekday

	startOffPeakHour = offPeakHourWeekday[1][0]
	print 'startOffPeakHourWeekday : %s' %startOffPeakHour
	endOffPeakHour = offPeakHourWeekday[2][0]
	print 'endOffPeakHourWeekday : %s' %endOffPeakHour

	print "----------------------PEAK HOUR WEEKDAY----------------------"
	startPeakWeekday = peakHourWeekday[0][0]
	print 'startPeakWeekday : %s' %startPeakWeekday
	endPeakWeekday = peakHourWeekday[0][1]
	print 'endPeakWeekday : %s' %endPeakWeekday

	startPeakHour = peakHourWeekday[1][0]
	print 'startPeakHour : %s' %startPeakHour
	endPeakHour = peakHourWeekday[2][0]
	print 'endPeakHour : %s' %endPeakHour

	print "----------------------PEAK HOUR WEEKEND----------------------"
	startPeakWeekend = peakHourWeekend[0][0]
	print 'startPeakWeekend : %s' %startPeakWeekend
	endPeakWeekend = peakHourWeekend[0][1]
	print 'endPeakWeekend : %s' %endPeakWeekend

	startPeakHourWeekend = peakHourWeekend[1][0]
	print 'startPeakHourWeekend : %s' %startPeakHourWeekend
	endPeakHourWeekend = peakHourWeekend[2][0]
	print 'endPeakHourWeekend : %s' %endPeakHourWeekend

	print "----------------------CRITIC PEAK HOUR END OF THE WEEK----------------------"
	startCriticWeekend = criticPeakHour[0][0]
	print 'startCriticWeekend : %s' %startCriticWeekend
	endCriticWeekend = criticPeakHour[0][1]
	print 'endCriticWeekend : %s' %endCriticWeekend

	startCriticPeakHour = criticPeakHour[1][0]
	print 'startCriticPeakHour : %s' %startCriticPeakHour
	endCriticPeakHour = criticPeakHour[2][0]
	print 'endCriticPeakHour : %s' %endCriticPeakHour


	"""
	Pour tester le changement de date
	Pacific/Auckland
	Europe/Paris
	"""

	print "----------------------HOW TIME - HOUR - DAY----------------------"
	#Les robots etant en UTC, la timezone choisi est celle de Paris
	ParisTimeZone = 'Europe/Paris'
	#heure a l instant t au debut du sccenario
	nowPARIStime = datetime.datetime.now(pytz.utc).astimezone(pytz.timezone(ParisTimeZone))
	print 'nowPARIStime = %s' %nowPARIStime
	#recuperation de l heure et du jour de la semaine
	currentHour = nowPARIStime.hour
	currentDay = nowPARIStime.weekday()
	print 'heure a l instant t : %s' %currentHour
	print 'jour a l instant t : %s' %currentDay

	"""
	Test
	currentHour = 22
	currentDay = 3
	"""

	print "----------------------CALCUL RUNSCORE----------------------"
	#Si la mesure a lieu entre lundi et vendredi
	if startOffPeakWeekday <= currentDay <= endOffPeakWeekday :
	    #Si la mesure a lieu vendredi entre 20h00 et 23h59
	    if currentDay == endOffPeakWeekday and startCriticPeakHour <= currentHour <= endCriticPeakHour:
	        runScore = highRate
	        print 'Le runScore est de %s' %runScore
	    #Si la mesure a lieu entre lundi et vendredi entre 00h00 et 19h59
	    elif startOffPeakHour <= currentHour <= endOffPeakHour:
	        runScore = lowRate
	        print 'Le runScore est de %s' %runScore
	    #Si la mesure a lieu entre lundi et jeudi entre 20h00 et 23h59
	    elif startPeakHour <= currentHour <= endPeakHour:
	        runScore = middleRate
	        print 'Le runScore est de %s' %runScore
	    else:
	        print "INPUT ERROR"

	#Si la mesure a lieu entre samedi et dimanche       
	elif startPeakWeekend <= currentDay <= endPeakWeekend:
	    #Si la mesure a lieu entre 00h00 et 19h59
	    if startPeakHourWeekend <= currentHour <= endPeakHourWeekend:
	        runScore = middleRate
	        print 'Le runScore est de %s' %runScore
	    #Si la mesure a lieu entre 20h00 et 23h59
	    elif startCriticPeakHour <= currentHour <= endCriticPeakHour:
	        runScore = highRate
	        print 'Le runScore est de %s' %runScore
	    else:
	        print "INPUT ERROR"

	#Si mauvais input
	else:
	    print "INPUT ERROR"

	ret_dict["runScore"] = runScore
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict