### begin wb_initialization
### end wb_initialization
### begin wb_common
### end wb_common
### begin wb_destroy
### end wb_destroy

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

@wb_export
def open_file(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"6395008a-d9ee-45bb-bf2d-6cb86220b933","in":{"chemin":"string","filename":"string","openingMode":"string"},"out":{},"comments":{"in":{"chemin":"","filename":"","openingMode":""},"out":{}}}"""
    # Open a CSV file


    #input :
    import cv



    #output :
    ret_dict = {}
    ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
    return ret_dict


@wb_export
def campaignPerf(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"e7608984-12dd-47ef-aa20-c475588d4372","in":{"rmcSportDuration":"duration","tf1Duration":"duration","fr2Duration":"duration","ocsDuration":"duration","rmcDecDuration":"duration","svodDuration":"duration","6playDuration":"duration","vodDuration":"duration","passAdulteDuration":"duration"},"out":{"campaignDuration":"duration"},"comments":{"in":{"rmcSportDuration":"","tf1Duration":"","fr2Duration":"","ocsDuration":"","rmcDecDuration":"","svodDuration":"","6playDuration":"","vodDuration":"","passAdulteDuration":""},"out":{"campaignDuration":""}}}"""
    # check if all scenarios are running successfully
    ret_dict = {}

    ret_dict["campaignDuration"] = d["ocsDuration"] + d["tf1Duration"] + d["fr2Duration"] + d["svodDuration"] + d["6playDuration"]  + d["vodDuration"] + d["rmcDecDuration"] + d["rmcSportDuration"] + d["passAdulteDuration"]

    ret_dict["returncode"] = 0 #success

    return ret_dict


