### begin wb_initialization
import cPickle, os, time
	
### end wb_initialization
### begin wb_common

class WPersistent():
    def __init__(self, myAlias=""):
        self.root_dir = os.path.join("c:\\","ProgramData","Witbe","storage","data")
        self.persistent_dir=os.path.join(self.root_dir,"persistent")
        print("myAlias in WPersistent : %s" %myAlias)
        self.db_file=os.path.join(self.persistent_dir,myAlias+".pickle")

        self.db={}
        if not os.path.exists(self.persistent_dir):
            os.makedirs(self.persistent_dir)
        if not os.path.exists(self.db_file):
            self.db["Creation date"]=time.ctime
            self.save()
        self.load()

    def load(self):
        fileObject = open(self.db_file,'rb')   
        self.db=cPickle.load(fileObject)
        fileObject.close()

    def save(self):
        fileObject = open(self.db_file,'wb+')   
        cPickle.dump(self.db,fileObject)
        fileObject.close()

    def test(self,name):
        if self.db.has_key(name):
            return True
        else:
            return False	

    def get(self, name):
       return self.db[name]

    def set(self,name,value):
        self.db[name]=value
        
### end wb_common
### begin wb_destroy
### end wb_destroy

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

@wb_export
def GetScenarioInformationFromFile(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"8c60dd93-2ce5-4149-9f81-04e56891164f","in":{},"out":{"myScenarioName":"string","myConsecutiveNumberOfError":"integer","myCurrentTime":"string","myLastRebootTime":"string"},"comments":{"in":{},"out":{"myScenarioName":"","myConsecutiveNumberOfError":"","myCurrentTime":"","myLastRebootTime":""}}}"""
	# Format
	#rebootStatus = { 
	#    { "myAlias" : "scriptName",
	#      "myLastRebootTime" : Last Reboot time,
	#      "myconsecutiveError" : nbre Of consecutiveError
	#      "myCurrentTime : Current system time"
	#    }
	#    }

	# Init Variables
	rebootStatus ={} # Variable from the file containing Scenario Informaion status
	myScriptPreviousInfo = {} # Inforamtion status of te sceanrio
	myScriptInfo={}
	ret_dict = {}
	# Open/Create File
	wdb=WPersistent(d["environment"]["alias"])

	# Check for Inforamtion
	if wdb.test("rebootStatus"):
	    myScriptPreviousInfo =  wdb.get("rebootStatus")
	else:
	    print "rebootStatus does not exists"

	# Debug
	#print("My Script Previous Info : %s" %myScriptPreviousInfo)

	#print("my Alias : %s" %myScriptPreviousInfo["myAlias"])
	#print("myConsecutiveError : %s" %myScriptPreviousInfo["myConsecutiveError"])
	#print("myCurrentTime :%s " %myScriptPreviousInfo["myCurrentTime"])


	# Define return values
	if myScriptPreviousInfo.get("myAlias") != None :
	    ret_dict["myScenarioName"]= myScriptPreviousInfo["myAlias"]
	else : 
	    print("Get the script name")
	    ret_dict["myScenarioName"]=d["environment"]["alias"]
	if myScriptPreviousInfo.get("myLastRebootTime") != None :
	    ret_dict["myLastRebootTime"] = str(myScriptPreviousInfo["myLastRebootTime"])
	else : 
	    print("Get the current time")
	    ret_dict["myLastRebootTime"] = str(time.time())

	if myScriptPreviousInfo.get("myConsecutiveError") != None :
	    ret_dict["myConsecutiveNumberOfError"] = int(myScriptPreviousInfo["myConsecutiveError"])
	else :
	    print("Intiialize to 0")
	    ret_dict["myConsecutiveNumberOfError"] = 0

	ret_dict["myCurrentTime"] = str(time.time())




	# Define return code
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict

@wb_export
def ChangeScenarioInformationToFile(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"79b4206d-8f5f-4b28-82ad-6dbb6cce0583","in":{"myAlias":"string","myCurrentTime":"string","myConsecutiveNumberOfError":"integer","myLastRebootTime":"string"},"out":{},"comments":{"in":{"myAlias":"","myCurrentTime":"","myConsecutiveNumberOfError":"","myLastRebootTime":""},"out":{}}}"""
	# Format
	#rebootStatus = { 
	#    { "myAlias" : "scriptName",
	#      "myLastRebootTime" : Last Reboot time,
	#      "myconsecutiveError" : nbre Of consecutiveError
	#      "myCurrentTime : Current system time"
	#    }
	#    }
	# Init Variables
	rebootStatus ={} # Variable from the file containing Scenario Informaion status
	myScriptPreviousInfo = {} # Inforamtion status of te sceanrio
	myScriptInfo={}
	ret_dict = {}

	print("myConsecutiveNumberOfError : %s" %d["myConsecutiveNumberOfError"])
	# Open/Create File
	wdb=WPersistent(d["environment"]["alias"])

	# Check for Inforamtion
	if wdb.test("rebootStatus"):
	    myScriptPreviousInfo =  wdb.get("rebootStatus")
	else:
	    print "rebootStatus does not exists"

	# Debug
	print("My Script Previous Info : %s" %myScriptPreviousInfo)


	# Change Information in the File
	myScriptInfo["myAlias"] = d["myAlias"]
	myScriptInfo["myLastRebootTime"] = d["myLastRebootTime"]
	myScriptInfo["myCurrentTime"] = d["myCurrentTime"]
	myScriptInfo["myConsecutiveError"] = d["myConsecutiveNumberOfError"]

	wdb.set("rebootStatus",myScriptInfo)
	wdb.save()


	# Define return code
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict

@wb_export
def GetRebootInformation(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"be5fd982-11c7-4a09-be90-cebad5e5516f","in":{"myDeviceName":"string"},"out":{"myRebootTime":"string"},"comments":{"in":{"myDeviceName":""},"out":{"myRebootTime":""}}}"""
	# Format
	#rebootStatus = { 
	#    { "myRebootTime : String (Last reboot time for the device)
	#    }
	#    }

	# Init Variables
	rebootStatus ={} # Variable from the file containing Scenario Informaion status
	myScriptPreviousInfo = {} # Inforamtion status of te sceanrio
	myScriptInfo={}
	ret_dict = {}
	# Open/Create File
	wdb=WPersistent(d["myDeviceName"])

	# Check for Inforamtion
	if wdb.test("rebootStatus"):
	    myScriptPreviousInfo =  wdb.get("rebootStatus")
	else:
	    print "rebootStatus does not exists"

	# Debug
	#print("My Script Previous Info : %s" %myScriptPreviousInfo)

	#print("my Alias : %s" %myScriptPreviousInfo["myAlias"])
	#print("myConsecutiveError : %s" %myScriptPreviousInfo["myConsecutiveError"])
	#print("myCurrentTime :%s " %myScriptPreviousInfo["myCurrentTime"])
	#print("myLastReboot : %s" %myScriptPreviousInfo["myRebootTime"])
	#print("reboot time from file : %s" %myScriptPreviousInfo.get("myRebbotTime"))


	print("toto : %s" %myScriptPreviousInfo)

	print("myRebootTime : %s" %myScriptPreviousInfo.get("myRebootTime"))
	# Define return values
	if myScriptPreviousInfo.get("myRebootTime") != None :
	    print("myLastReboot : %s" %myScriptPreviousInfo["myRebootTime"])
	    ret_dict["myRebootTime"]= myScriptPreviousInfo["myRebootTime"]
	else : 
	    print("Get the reboot time")
	    ret_dict["myRebootTime"] = str(time.time())


	# Define return code
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict


@wb_export
def ChangeRebootInformation(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"44edf4a4-5a4d-4275-83e8-c351763c8285","in":{"myDeviceName":"string","myRebootTime":"string"},"out":{},"comments":{"in":{"myDeviceName":"","myRebootTime":""},"out":{}}}"""
	# Format
	#rebootStatus = { 
	#    { "myRebootTime : String (Time last reboot of the device)
	#    }
	#    }
	# Init Variables
	rebootStatus ={} # Variable from the file containing Scenario Informaion status
	myScriptPreviousInfo = {} # Inforamtion status of te sceanrio
	myScriptInfo={}
	ret_dict = {}

	# Open/Create File
	wdb=WPersistent(d["myDeviceName"])

	# Check for Inforamtion
	if wdb.test("rebootStatus"):
	    myScriptPreviousInfo =  wdb.get("rebootStatus")
	else:
	    print "rebootStatus does not exists"

	# Debug
	print("My Script Previous Info : %s" %myScriptPreviousInfo)


	# Change Information in the File
	myScriptInfo["myRebootTime"] = d["myRebootTime"]
	wdb.set("rebootStatus",myScriptInfo)
	wdb.save()


	# Define return code
	ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	return ret_dict


@wb_export
def GetVideoTraceLink(jsonstr, d={}):
	# Definition of Input and Output Parameters (name and type)
	"""{"id":"310f6032-8610-4d2e-b333-86865c097e12","in":{},"out":{"capturePath":"string"},"comments":{"in":{},"out":{"capturePath":""}}}"""
	# Created by FGHA
	# Edited by FGHA 12/03/2018 - Get Video trace path

	import os
	import datetime
	from datetime import datetime
	import glob
	import json
	import urllib2 
	from urllib2 import Request, urlopen

	ret_dict = {}

	# STEP 1 Retrieve robot name
	robot_name = ''
	## Robots list
	robots_list = [
					{"name" : 'mini-4HD-01', "uuid" : '87ec2168-5d84-42ad-99eb-6a8caa187cd7'}
					]

	## Get current robot uuid by requesting the robot's API

	response = urlopen(Request('http://127.0.0.1/api/v2/')).read() 
	response_json = json.loads(response) 
	robot_uuid = response_json["uuid"].encode("ascii")

	print robot_uuid

	## Get current robot name

	for robots in robots_list:
		if robots["uuid"] == robot_uuid:
			robot_name = robots["name"]
			print("robot name found. Exit") 
			break
		else:
			print("Robot name could not be retrieved. Exit block on the failure pad")
			print("Check if this robot uuid",robot_uuid," is in the robots_list")
			ret_dict["returncode"] = 1


	# STEP 2 Get frame trace URL

	## Get location of robot's storage
	path_to_traces = os.getenv('WITBETRACESSTORAGE')
	print path_to_traces

	## Get current time to build URL
	now = datetime.now()
	print now

	year = str(now.year)
	month = str(now.month)
	if len(month) == 1:
	    Omonth = "0" + month
	else:
		Omonth = month

	day = str(now.day)
	if len(day) == 1:
	    Oday = "0" + day
	else:
		Oday = day  

	hour = str(now.hour)
	if len(hour) == 1:
	    Ohour = "0" + hour
	else:
		Ohour = hour

	minute = str(now.minute)
	if len(minute) == 1:
	    Ominute = "0" + minute
	else:
		Ominute = minute

	## Build URL to find the Capture name on the robot
	###Works for 6.1.3.4

	#path_to_find_capture = path_to_traces + "\\" + "video" + "\\" + year + "\\" + month + "\\" + day + "\\" + year + Omonth + Oday + Ohour + Ominute + "*.mkv"

	###Works for 6.2.0
	#path_to_find_capture = path_to_traces + "\\" + year + "\\" + month + "\\" + day + "\\" + "video" + "\\" + year + Omonth + Oday + Ohour + Ominute + "*.mkv"
	path_to_find_capture = path_to_traces + "\\" + year + "\\" + Omonth + "\\" + Oday + "\\" + "video" + "\\" + year + Omonth + Oday + Ohour + "*.mkv"

	print("path to find capture in directory:  " ,path_to_find_capture )

	## Build the URL to report on Datalab. External Direct link to the robot
	try:
		pathlist = glob.glob(path_to_find_capture)
		print pathlist
		path_to_capture_long = pathlist[-1]  #select last result only
		print("path of the capture:  ",path_to_capture_long )
		capture_name = path_to_capture_long.split('\\')[5] #select capture name only from long path
		print("capture name is: ", capture_name)
		#Works for 6.1.3.4
		#download_link = "https://aetn.cust.witbe.net/robots/" + robot_name + "/static/data/traces/video/" + year + "/" + month + "/" + day + "/" + capture_name
		#Works for 6.2.0
		download_link = "http://93.26.66.67/robots/" + robot_name + "/static/data/traces/" + year + "/" + Omonth + "/" + Oday + "/" + "video/" + capture_name
		ret_dict["capturePath"] = download_link
		ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
	except:
		print("no capture found, index out of range")
		ret_dict["capturePath"] = "no capture found"
		ret_dict["returncode"] = 1 

	return ret_dict 
