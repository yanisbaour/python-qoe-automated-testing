### begin wb_initialization
### end wb_initialization
### begin wb_common
### end wb_common
### begin wb_destroy
### end wb_destroy

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

def wb_export(f):
    import json, inspect
    params = json.loads(f.__doc__)
    if "in" not in params and "out" not in params:
        raise ValueError("malformed parameter")
    try:
        #remove type validation
        pass
        #if "value" not in params['out']:
        #    raise ValueError("out should contain value")
        #for k, v in params['in'].iteritems():
        #    if (not isinstance(v, basestring)) and (not isinstance(v, int)) \
        #            and (not isinstance(v, dict)) and (not isinstance(v, list)) \
        #            and (not isinstance(v, bool)) and (not isinstance(v, float)):
        #        raise ValueError("not string")
    except AttributeError as e:
        raise ValueError(e)
    except TypeError as e:
        raise ValueError(e)
    __wb_export = True
    def json_wrapper(input):
        input_d = json.loads(input.decode('utf8'))
        if 'd' in inspect.getargspec(f).args:
            return_value = f(input, d=input_d)
        else:
            return_value = f(input)
        if isinstance(return_value, str):
            return return_value
        else:
            return json.dumps(return_value)
    json_wrapper.__wb_export = __wb_export
    json_wrapper.__doc__ = getattr(f, '__doc__', None)

    return json_wrapper

@wb_export
def CompareTime(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"6d0d6766-c069-410f-9528-197d5b69976b","in":{"previousTime":"string","currentTime":"string","diffTimeToReboot":"string"},"out":{},"comments":{"in":{"previousTime":"seconds since the epoch","currentTime":"seconds since the epoch","diffTimeToReboot":"in Seconds"},"out":{}}}"""
    # We are comparing diffTimeTo reboot with the last reboot time and the current time
    # reutning 1 means : No Need to reboot yet
    # return 0 : Needs to reboot now (in case myConsecutiveNumberOfError reached 3)
    ret_dict = {}

    if float(d["diffTimeToReboot"]) > (float(d["currentTime"]) - float(d["previousTime"])) :
        print("Reboot has not been done yet")
        ret_dict["returncode"] = 1
    else :
        print("Reboot need to be done, the nwe are going to Reset the file information")
        ret_dict["returncode"] = 0

    return ret_dict


@wb_export
def FormatTime(jsonstr, d={}):
    # Definition of Input and Output Parameters (name and type)
    """{"id":"4d46a611-b75e-4419-b82d-44a2d2332c22","in":{"myTimeInSecond":"string"},"out":{"myTimeFormat":"string"},"comments":{"in":{"myTimeInSecond":""},"out":{"myTimeFormat":""}}}"""
    import time
    ret_dict = {}


    ret_dict["myTimeFormat"] = time.ctime(float(d["myTimeInSecond"]))

    ret_dict["returncode"] = 0 # set to 1 to output on the FAILURE output
    return ret_dict